/**
 * Created by Jordan Gauthier on 2016-07-06.
 */


var img1 = new Image();
img1.src = 'ammopn.png';


var img2 = new Image();
img2.src = 'speed.png';


var img3 = new Image();
img3.src = 'popohp.png';


function Game(ctx,ctxback,ctxBullet,ctxUI,ctxBonus,ctx2,ctxBullet2,ctxMenu)
{
    //le context
    this.context = ctx; //context du ninja 1
    this.ctxback = ctxback; //context du background et platform
    this.ctxbullet = ctxBullet; //context des balle du player 1
    this.ctxUI = ctxUI; //context de tous se qui est UI
    this.ctxBonus = ctxBonus; //context des bonus
    this.ctx2 = ctx2;
    this.ctxBullet2 = ctxBullet2;
    this.ctxMenu = ctxMenu;
    this.resultList = new ResultList();
    this.resultList.afficher();

    this.isPause = false;
    
    this.bonusTab = []; //tableau contenant les bonus
    
    this.menu = new Menu(this.ctxMenu)//instance de menu
    this.menu.add(new Button(1024/2-200,250,400,50,"Play",0,this.ctxMenu));
    this.menu.add(new Button(1024/2-200,350,400,50,"Option",1,this.ctxMenu));
    this.menu.add(new Button(1024/2-200,450,400,50,"Rules",2,this.ctxMenu));

    this.platformTab = []; //tableau qui contient tous les platforms
    this.platformTab[0] = new Platform(50,hback-50,320,3,this.ctxback); //platform 1
    this.platformTab[1] = new Platform(wback-370,hback-50,320,3,this.ctxback);//platform 2
    this.platformTab[2] = new Platform(wback/2-210,hback-150,420,3,this.ctxback);//platform 3
    this.platformTab[3] = new Platform(50,hback-250,250,3,this.ctxback);//platform 4
    this.platformTab[4] = new Platform(wback-300,hback-250,250,3,this.ctxback);//platform 5
    this.platformTab[5] = new Platform(wback/2-210,hback-350,420,3,this.ctxback);//platform 6
    this.platformTab[6] = new Platform(50,hback-450,320,3,this.ctxback);//platform 7
    this.platformTab[7] = new Platform(wback-370,hback-450,320,3,this.ctxback);//platform 8

    var back = new Image();
    back.src = "background_tp_js.png";
    back.onload = function ()
    {
        ctxback.drawImage(this, 0, 0, back.width, back.height, 0, 0, w, h);

        for(var i = 0 ; i <= game.platformTab.length-1 ; i++)
        {
            r = new Rectangle(game.platformTab[i].x,game.platformTab[i].y,game.platformTab[i].width,game.platformTab[i].height,ctxback,true,"ghostwhite",false,"black");
            r.draw();
        }
    };

    //instance du premier ninja
    this.n1 = new Ninja(50,300,this.context,this.ctxbullet);
    this.n1.righSpriteWay = ["sprites_ninja_noir/spritesn04.png","sprites_ninja_noir/spritesn05.png","sprites_ninja_noir/spritesn06.png"];
    this.n1.leftSpriteWay = ["sprites_ninja_noir/spritesn01.png","sprites_ninja_noir/spritesn02.png","sprites_ninja_noir/spritesn03.png"];
    this.posN1 = 0;//position du rendu des sprites du ninj1

    //instance du 2ieme ninja
    this.n2 = new Ninja(1024-100,300,this.ctx2,this.ctxBullet2);
    this.n2.righSpriteWay = ["sprites_ninja_rouge/sprite01.png","sprites_ninja_rouge/sprite02.png","sprites_ninja_rouge/sprite03.png"];
    this.n2.leftSpriteWay = ["sprites_ninja_rouge/sprite04.png","sprites_ninja_rouge/sprite05.png","sprites_ninja_rouge/sprite06.png"];
    this.posN2 = 0; //position du rendu des sprite du ninja 2


    this.lb1 = new LifeBar(10,10,300,20,this.n1,"Player 1",this.ctxUI); //barre de vie du player 1
    this.lb2 = new LifeBar(1024-320,10,300,20,this.n2,"Player 2",this.ctxUI); //barre de vie du player 2

    this.score = new Score(475,50,18,this.ctxUI); //instance du score
    
    this.gameInterval = null; //interval qui va contenir la boucle du jeu
    this.isFinish = false;
    this.isAfterGame = false;
    this.isStarted = false; //si la game es commencer
    this.winner = null; //va contenir le gagnant de la parti

    this.timer = 5;

    this.collision = false;
    
    this.MenuPlay = function()
    {
        if(!this.isStarted)
            this.menu.draw();
    };

    this.AfterGameMenuPlay = function()
    {
        this.menu.drawAfterGameMenu(this.winner);
    };
    
    this.resetGame = function()
    {
        //instance du premier ninja
        this.n1 = new Ninja(50,300,this.context,this.ctxbullet);
        this.n1.righSpriteWay = ["sprites_ninja_noir/spritesn04.png","sprites_ninja_noir/spritesn05.png","sprites_ninja_noir/spritesn06.png"];
        this.n1.leftSpriteWay = ["sprites_ninja_noir/spritesn01.png","sprites_ninja_noir/spritesn02.png","sprites_ninja_noir/spritesn03.png"];
        this.posN1 = 0;//position du rendu des sprites du ninj1

        //instance du 2ieme ninja
        this.n2 = new Ninja(1024-100,300,this.ctx2,this.ctxBullet2);
        this.n2.righSpriteWay = ["sprites_ninja_rouge/sprite01.png","sprites_ninja_rouge/sprite02.png","sprites_ninja_rouge/sprite03.png"];
        this.n2.leftSpriteWay = ["sprites_ninja_rouge/sprite04.png","sprites_ninja_rouge/sprite05.png","sprites_ninja_rouge/sprite06.png"];
        this.posN2 = 0; //position du rendu des sprite du ninja 2


        this.lb1 = new LifeBar(10,10,300,20,this.n1,"Player 1",this.ctxUI); //barre de vie du player 1
        this.lb2 = new LifeBar(1024-320,10,300,20,this.n2,"Player 2",this.ctxUI); //barre de vie du player 2

        this.score = new Score(475,50,18,this.ctxUI); //instance du score
        
        this.gameInterval = null;
        
        clearInterval(game.n1.intervalMove);
        clearInterval(game.n2.intervalMove);
        clearInterval(game.n2.intervalSpeed);
        clearInterval(game.n1.intervalSpeed);

        this.timer = 5;

        this.ctxBonus.clearRect(0,0,1024,768);
        this.bonusTab = [];
    };

    this.startGame = function()
    {
        var drop = false;
        var self = this;
        var mod = 1;

        var startTimer = setInterval(function()
        {
            self.timer--;
            
            if(self.timer <= 0)
                clearInterval(startTimer);
        },1000);

        this.gameInterval = setInterval(function()
        {
            self.ctxMenu.clearRect(0,0,1024,768);
            self.isStarted = true;
            mod++;
            self.ctxUI.clearRect(0,0,1024,768);
            self.lb1.draw();
            self.lb2.draw();
            self.score.draw();

            if(self.timer > 0)
            {
                self.context.clearRect(0,0,1024,768);
                self.ctx2.clearRect(0,0,1024,768);
                self.context.font ="50px Arial";
                self.context.fillText(self.timer,1024/2,768/2);
            }
            else
            {
                if(self.isPause)
                {
                    clearInterval(self.n1.intervalMove);
                    clearInterval(self.n2.intervalMove);
                    clearInterval(self.gameInterval);
                    self.menu.drawPause();
                }

                if(self.score.scoreP2 == 3 || self.score.scoreP1 == 3)
                {
                    self.isAfterGame = true;

                    if(self.score.scoreP2 == 3 && self.score.scoreP1 == 3)
                        self.winner = "Draw";
                    else if(self.score.scoreP1 == 3)
                        self.winner = "Player 1";
                    else if(self.score.scoreP2 == 3)
                        self.winner = "Player 2";

                    self.resultList.add(new GameResult(self.score.scoreP1,self.score.scoreP2));

                    
                    clearInterval(self.n1.intervalMove);
                    clearInterval(self.n1.intervalSpeed);
                    clearInterval(self.n2.intervalMove);
                    clearInterval(self.n2.intervalSpeed);
                    clearInterval(self.gameInterval);
                    self.isStarted = false;
                    self.isAfterGame = true;
                    self.resetGame();
                    self.AfterGameMenuPlay();
                }

                self.ctxUI.font="20px Arial";
                self.ctxUI.fillText("Shuriken : "+self.n1.chargeur,10,55);
                self.ctxUI.fillText("Shuriken : "+self.n2.chargeur,885,55);

                var nb = Math.round((Math.random() * 1));

                if(mod % 2000 == 1)
                {
                    var audio = new Audio('interface6.wav');
                    audio.play();
                    self.generateBonus(nb);
                    self.generateBonus(2);
                    self.generateBonus(2);
                }

                //joueur1
                if(self.n1.goLeft)
                {
                    if(!self.n1.moveStart)
                    {
                        self.n1.moveStart = true;

                        self.n1.intervalMove = setInterval(function()
                        {
                            self.n1.x-=self.n1.speed-1;
                            self.n1.draw(self.posN1,self.n1.x,self.n1.y);
                            self.posN1++;

                            if(self.posN1 == 3)
                                self.posN1 = 0;
                        },6);
                    }
                }

                if(self.n1.goRight)
                {
                    if(!self.n1.moveStart)
                    {
                        self.n1.moveStart = true;

                        self.n1.intervalMove = setInterval(function()
                        {
                            self.n1.x+=self.n1.speed-1;
                            self.n1.draw(self.posN1,self.n1.x,self.n1.y);
                            self.posN1++;

                            if(self.posN1 == 3)
                                self.posN1 = 0;
                        },6);
                    }
                }

                for(var i = 0 ; i <=  self.platformTab.length-1 ; i++)
                {
                    if(((self.n1.x+self.n1.width-8) <  self.platformTab[i].x ) || (self.n1.x > ( self.platformTab[i].x+ self.platformTab[i].width-8)) || !(self.n1.y + self.n1.height >=  self.platformTab[i].y && self.n1.y + self.n1.height <=  self.platformTab[i].y +  self.platformTab[i].height))
                    {
                        self.n1.isDroping = true;
                    }
                    else
                    {
                        if(self.n1.isAlive)
                        {
                            self.n1.isDroping = false;
                            self.n1.y =  self.platformTab[i].y - self.n1.height;
                            self.n1.canJump = true;
                            self.n1.jumpCount = 0;
                            break;
                        }
                    }
                }


                for(var i = 0 ; i <= self.platformTab.length-1 ; i++)
                {
                    if((self.n1.x >=  self.platformTab[i].x-20) && (self.n1.x+self.n1.width <=  self.platformTab[i].x+ self.platformTab[i].width+20))
                    {
                        if((self.n1.y <=  self.platformTab[i].y+ self.platformTab[i].height) && (self.n1.y+self.n1.height >=  self.platformTab[i].y+ self.platformTab[i].height))
                        {
                            self.n1.isJumping = false;
                        }

                    }
                }

                if(self.n1.isDroping)
                    self.n1.drop();

                if(self.n1.y >= 900)
                {
                    clearInterval(self.n1.intervalMove);
                    clearInterval(self.n1.intervalSpeed);
                    self.n1.die();
                    self.score.scoreP2++;
                    self.n1.reset();
                }

                self.n1.updateShuriken();


                if (self.collision) {
                    self.ctxBonus.clearRect(0, 0, 1024, 768);
                    self.collision = false;
                }

                for(var i = 0 ; i <= self.bonusTab.length-1 ; i++)
                    self.bonusTab[i].draw();


                for(var i = 0 ; i <= self.bonusTab.length-1 ; i++)
                {
                    if(self.bonusTab[i].isAlive)
                    {
                        if(((self.n1.x + self.n1.width) >= self.bonusTab[i].x) && (self.n1.x <= self.bonusTab[i].x + self.bonusTab[i].width-20))
                        {
                            if(((self.n1.y + self.n1.height) >= self.bonusTab[i].y) && (self.n1.y <= self.bonusTab[i].y + self.bonusTab[i].height-20))
                            {
                                if(self.bonusTab[i].type == "shuriken")
                                {
                                    var audio = new Audio('Drop Sword-SoundBible.com-768774345.mp3');
                                    audio.play();
                                    self.n1.chargeur += 10;
                                }
                                else if(self.bonusTab[i].type == "health")
                                {

                                    var audio = new Audio('bubble2.wav');
                                    audio.play();

                                    self.n1.life += 40;

                                    if(self.n1.life > 300)
                                        self.n1.life = 300;
                                }
                                else if(self.bonusTab[i].type == "speed")
                                {
                                    var audio = new Audio('spell.wav');
                                    audio.play();

                                    if(!self.n1.isSpeed)
                                    {
                                        self.n1.speed+=1;
                                        self.n1.isSpeed = true;

                                        self.n1.intervalSpeed = setInterval(function()
                                        {
                                            if(self.n1.speedSecond >= 4)
                                            {
                                                clearInterval(self.n1.intervalSpeed);
                                                self.n1.speed-=1;
                                                self.n1.isSpeed = false;
                                                self.n1.speedSecond = 0;
                                            }

                                            self.n1.speedSecond++;

                                        },1000);
                                    }
                                    else
                                        self.n1.speedSecond = 0;
                                }

                                self.collision = true;
                                self.bonusTab[i].isAlive = false;
                            }
                        }
                    }
                }

                for(var i = 0 ; i <= self.n1.shurikenTab.length-1 ; i++)
                {
                    if(self.n1.shurikenTab[i].isAlive)
                    {
                        if((self.n1.shurikenTab[i].x >= self.n2.x) && (self.n1.shurikenTab[i].x + self.n1.shurikenTab[i].width <= self.n2.x + self.n2.width))
                        {
                            if((self.n1.shurikenTab[i].y >= self.n2.y) && (self.n1.shurikenTab[i].y + self.n1.shurikenTab[i].height <= self.n2.y + self.n2.height))
                            {
                                if(self.n2.life > 0)
                                    self.n2.life -= 25;

                                if(self.n2.life <= 0)
                                    self.n2.die();

                                self.n1.shurikenTab[i].isAlive = false;
                            }
                        }
                    }
                }

                //joueur2
                if(self.n2.goLeft)
                {
                    if(!self.n2.moveStart)
                    {
                        self.n2.moveStart = true;

                        self.n2.intervalMove = setInterval(function()
                        {
                            self.n2.x-=self.n2.speed-1;
                            self.n2.draw(self.posN2,self.n2.x,self.n2.y);
                            self.posN2++;

                            if(self.posN2 == 3)
                                self.posN2 = 0;
                        },6);
                    }
                }

                if(self.n2.goRight)
                {
                    if(!self.n2.moveStart)
                    {
                        self.n2.moveStart = true;

                        self.n2.intervalMove = setInterval(function()
                        {
                            self.n2.x+=self.n2.speed-1;
                            self.n2.draw(self.posN2,self.n2.x,self.n2.y);
                            self.posN2++;

                            if(self.posN2 == 3)
                                self.posN2 = 0;
                        },6);
                    }
                }

                for(var i = 0 ; i <=  self.platformTab.length-1 ; i++)
                {
                    if(((self.n2.x+self.n2.width-8) <  self.platformTab[i].x ) || (self.n2.x > ( self.platformTab[i].x+ self.platformTab[i].width-8)) || !(self.n2.y + self.n2.height >=  self.platformTab[i].y && self.n2.y + self.n2.height <=  self.platformTab[i].y +  self.platformTab[i].height))
                    {
                        self.n2.isDroping = true;
                    }
                    else
                    {
                        if(self.n2.isAlive)
                        {
                            self.n2.isDroping = false;
                            self.n2.y =  self.platformTab[i].y - self.n2.height;
                            self.n2.canJump = true;
                            self.n2.jumpCount = 0;
                            break;
                        }
                    }
                }

                for(var i = 0 ; i <= self.platformTab.length-1 ; i++)
                {
                    if((self.n2.x >=  self.platformTab[i].x-20) && (self.n2.x+self.n2.width <=  self.platformTab[i].x+ self.platformTab[i].width+20))
                    {
                        if((self.n2.y <=  self.platformTab[i].y+ self.platformTab[i].height) && (self.n2.y+self.n2.height >=  self.platformTab[i].y+ self.platformTab[i].height))
                        {
                            self.n2.isJumping = false;
                        }

                    }
                }

                if(self.n2.isDroping)
                    self.n2.drop();

                if(self.n2.y >= 900)
                {
                    clearInterval(self.n2.intervalMove);
                    clearInterval(self.n2.intervalSpeed);
                    self.n2.die();
                    self.score.scoreP1++;
                    self.n2.reset();
                }

                self.n2.updateShuriken();

                for(var i = 0 ; i <= self.bonusTab.length-1 ; i++)
                {
                    if(self.bonusTab[i].isAlive)
                    {
                        if(((self.n2.x + self.n2.width) >= self.bonusTab[i].x) && (self.n2.x <= self.bonusTab[i].x + self.bonusTab[i].width-20))
                        {
                            if(((self.n2.y + self.n2.height) >= self.bonusTab[i].y) && (self.n2.y <= self.bonusTab[i].y + self.bonusTab[i].height-20))
                            {
                                if(self.bonusTab[i].type == "shuriken")
                                {
                                    var audio = new Audio('Drop Sword-SoundBible.com-768774345.mp3');
                                    audio.play();
                                    self.n2.chargeur += 10;
                                }
                                else if(self.bonusTab[i].type == "health")
                                {

                                    var audio = new Audio('bubble2.wav');
                                    audio.play();

                                    self.n2.life += 40;

                                    if(self.n2.life > 300)
                                        self.n2.life = 300;
                                }
                                else if(self.bonusTab[i].type == "speed")
                                {
                                    var audio = new Audio('spell.wav');
                                    audio.play();

                                    if(!self.n2.isSpeed)
                                    {
                                        self.n2.speed+=1;
                                        self.n2.isSpeed = true;

                                        self.n2.intervalSpeed = setInterval(function()
                                        {
                                            if(self.n2.speedSecond >= 4)
                                            {
                                                clearInterval(self.n2.intervalSpeed);
                                                self.n2.speed-=1;
                                                self.n2.isSpeed = false;
                                                self.n2.speedSecond = 0;
                                            }

                                            self.n2.speedSecond++;

                                            console.log(self.n2.speedSecond);
                                        },1000);
                                    }
                                    else
                                        self.n2.speedSecond = 0;
                                }

                                self.collision = true;
                                self.bonusTab[i].isAlive = false;
                            }
                        }
                    }
                }

                for(var i = 0 ; i <= self.n2.shurikenTab.length-1 ; i++)
                {
                    if(self.n2.shurikenTab[i].isAlive)
                    {
                        if((self.n2.shurikenTab[i].x >= self.n1.x) && (self.n2.shurikenTab[i].x + self.n2.shurikenTab[i].width <= self.n1.x + self.n1.width))
                        {
                            if((self.n2.shurikenTab[i].y >= self.n1.y) && (self.n2.shurikenTab[i].y + self.n2.shurikenTab[i].height <= self.n1.y + self.n1.height))
                            {
                                if(self.n1.life > 0)
                                    self.n1.life -= 25;

                                if(self.n1.life <= 0)
                                    self.n1.die();

                                self.n2.shurikenTab[i].isAlive = false;
                            }
                        }
                    }
                }
            }
        },0);
    };

    this.generateBonus = function(nb)
    {
        var p = Math.round(Math.random() * 7);
        var type = "";

        if(nb == 0)
            type = "speed";
        else if(nb == 1)
            type = "health";
        else if(nb == 2)
            type = "shuriken";

        this.bonusTab.push(new Bonus(this.platformTab[p].x+(Math.random() * (this.platformTab[p].width - 20)),this.platformTab[p].y-45,40,40,type,this.ctxBonus));
    }
    
    this.reset = function()
    {
        //instance du premier ninja
        this.n1 = new Ninja(50,300,this.context,this.ctxbullet);
        this.n1.righSpriteWay = ["sprites_ninja_noir/spritesn04.png","sprites_ninja_noir/spritesn05.png","sprites_ninja_noir/spritesn06.png"];
        this.n1.leftSpriteWay = ["sprites_ninja_noir/spritesn01.png","sprites_ninja_noir/spritesn02.png","sprites_ninja_noir/spritesn03.png"];
        this.posN1 = 0;//position du rendu des sprites du ninj1

        //instance du 2ieme ninja
        this.n2 = new Ninja(300,300,this.ctx2,this.ctxBullet2);
        this.n2.righSpriteWay = ["sprites_ninja_rouge/sprite01.png","sprites_ninja_rouge/sprite02.png","sprites_ninja_rouge/sprite03.png"];
        this.n2.leftSpriteWay = ["sprites_ninja_rouge/sprite04.png","sprites_ninja_rouge/sprite05.png","sprites_ninja_rouge/sprite06.png"];
        this.posN2 = 0; //position du rendu des sprite du ninja 2

        this.bonusTab = [];
    }

}

function Ninja(x,y,context,ctxbullet)
{
    this.x = x; //position x du ninja
    this.y = y; //position y du ninja
    this.width = 40; //largeur du ninja
    this.height = 50; //hauteur du ninja
    this.isAlive = true; //si le ninja es en vie
    this.canJump = true; //si le ninja peu oui ou non sauter
    this.isJumping = false ;//si le ninja es entrain de sauter
    this.shurikenTab = []; //tableau qui contient les shuriken du ninja
    this.life = 300; //vie du ninja;
    this.leftSpriteWay = []; //sprite quand le ninja va a gauche
    this.righSpriteWay = []; //sprite quand le ninja va a droite
    this.way = "-1"; //si le ninja va a gauche vaut true a droite false
    this.goLeft = false;
    this.goRight = false;
    this.isDroping = false;
    this.moveStart = false;
    this.intervalMove = null;
    this.context = context; //le context du canvas
    this.way = null;
    this.jumpStartY = null;
    this.lastWay = null;
    this.canShoot = true;
    this.jumpCount = 0;
    this.shootTime = 0;
    this.ctxbullet = ctxbullet;
    this.chargeur = 10;
    this.speed = 3;
    this.isSpeed = false;
    this.intervalSpeed = null;
    this.speedSecond = 0;

    this.reset = function()
    {
        this.x = x; //position x du ninja
        this.y = y; //position y du ninja
        this.width = 40; //largeur du ninja
        this.height = 50; //hauteur du ninja
        this.isAlive = true; //si le ninja es en vie
        this.canJump = true; //si le ninja peu oui ou non sauter
        this.isJumping = false ;//si le ninja es entrain de sauter
        this.shurikenTab = []; //tableau qui contient les shuriken du ninja
        this.life = 300; //vie du ninja;
        this.goLeft = false;
        this.goRight = false;
        this.isDroping = false;
        this.moveStart = false;
        this.intervalMove = null;
        this.way = null;
        this.jumpStartY = null;
        this.lastWay = null;
        this.canShoot = true;
        this.jumpCount = 0;
        this.chargeur = 10;
        this.speed = 3;
        this.isSpeed = false;
        this.intervalSpeed = null;
        this.speedSecond = 0;
    };

    this.draw = function(spriteNumber,x,y)
    {
        var nw = this.width;
        var nh = this.height;
        var self = this;
        if(this.goLeft)
        {
            var monImg = new Image();
            monImg.src = this.leftSpriteWay[spriteNumber];
            monImg.onload = function ()
            {
                context.clearRect(0,0,1024,768);
                self.context.drawImage(this,x,y,nw,nh);
            };
        }

        if(this.goRight)
        {
            var monImg = new Image();

            monImg.src = this.righSpriteWay[spriteNumber];
            monImg.onload = function ()
            {
                context.clearRect(0,0,1024,768);
                self.context.drawImage(this,x,y,nw,nh);
            };
        }

        if(!this.isJumping)
        {
            if(this.isDroping)
            {
               // this.canJump = false;

                if(this.moveStart)
                    this.y+= 2;
                else
                    this.y+=3;

                var monImg = new Image();

                if(this.way == "right")
                    monImg.src = this.righSpriteWay[spriteNumber];
                else if(this.way == "left")
                    monImg.src = this.leftSpriteWay[spriteNumber];
                else
                    monImg.src = this.leftSpriteWay[0];

                monImg.onload = function ()
                {
                    context.clearRect(0,0,1024,768);
                    self.context.drawImage(this,x,y,nw,nh);
                };
            }
        }

        if(this.isJumping)
        {
            if(this.moveStart)
                this.y-= 2;
            else
                this.y-=3;

            var monImg = new Image();

            if(this.way == "right")
                monImg.src = this.righSpriteWay[spriteNumber];
            else if(this.way == "left")
                monImg.src = this.leftSpriteWay[spriteNumber];
            else
                monImg.src = this.leftSpriteWay[0];

            monImg.onload = function ()
            {
                context.clearRect(0,0,1024,768);
                context.drawImage(this,x,y,nw,nh);
            };


            if(this.y <= this.jumpStartY-150)
            {
               this.isJumping = false;
            }
        }
    };

    this.shoot = function()
    {
        var self = this;

        if(this.canShoot && this.chargeur > 0)
        {
            var audio = new Audio('Arrow-SoundBible.com-1760405458.mp3');
            audio.play();
            this.shurikenTab.push(new Shuriken(this.x,this.y+((this.height/2)-5),this.lastWay,this.ctxbullet));
            this.canShoot = false;
            this.chargeur--;
        }
    };

    this.updateShuriken = function()
    {
        this.ctxbullet.clearRect(0,0,1024,768);

        for(var  i = 0 ; i <= this.shurikenTab.length-1 ; i++)
        {
            if(this.shurikenTab[i].isAlive)
            {
                if(this.shurikenTab[i].way ==  "right")
                    this.shurikenTab[i].x+=2;
                else if(this.shurikenTab[i].way ==  "left")
                    this.shurikenTab[i].x-=2;

                this.shurikenTab[i].draw();
            }
        }
    };

    this.jump = function()
    {
        if(this.canJump)
        {
            var audio = new Audio('jump_11.wav');
            audio.play();
            this.jumpStartY = this.y;
            this.isJumping = true;
            //this.canJump = false;
            this.jumpCount++;
            this.draw(0,this.x,this.y);

            if(this.jumpCount >= 2)
            {
                this.canJump = false;
            }

        }

    };

    this.drop = function()
    {
        this.isDroping = true;

        if(this.goLeft)
            this.way = "left";
        else if(this.goRight)
            this.way = "right";

        this.draw(0,this.x,this.y);
    }

    this.die = function()
    {
        if(this.isAlive)
        {
            var audio = new Audio('Hl2_Rebel-Ragdoll485-573931361.mp3');
            audio.play();
            this.isAlive = false;
        }
    }
}

function Shuriken(x,y,way,context)
{
    this.x = x; //position x du shuriken
    this.y = y; //position y du shuriken
    this.width = 10; //largeur du shuriken
    this.height = 10; //hauteur du shuriken
    this.isAlive = true; //si la balle est en vie
    this.way = way; //direction du shuriken
    this.context = context; //le context du canvas

    this.draw = function()
    {
        this.context.fillStyle = "red";
        this.context.fillRect(this.x,this.y,10,10);
    };
}

function Platform(x,y,width,height,context)
{
    this.x = x; //position x de la plateform
    this.y = y; //position y de la platform
    this.width = width; //largeur de la platform
    this.height = height; //hauteur de la platform
    this.haveUrl = false; //si oui ou non la platofrma un url
    this.url = null; //url de limage de la platform
    this.context = context; //le context du canvas

    this.draw = function()
    {

    };
}

function Bonus(x,y,width,height,type,context)
{
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.context = context;
    this.isAlive = true;
    this.type = type;

    this.draw = function()
    {
        if(this.isAlive)
        {
            if(this.type == "shuriken")
            {
                var self = this;
               // var speedImg = new Image();
                //speedImg.src = 'ammopn.png';
                //speedImg.onload = function()
               // {
                   self.context.drawImage(img1,self.x,self.y,self.width,self.height);
               // }
            }
            else if(this.type == "speed")
            {
                var self = this;
               // var speedImg = new Image();
              //  speedImg.src = 'speed.png';
              //  speedImg.onload = function()
              //  {
                    self.context.drawImage(img2,self.x,self.y,self.width,self.height);
                //}
            }
            else if(this.type == "stun")
                this.context.fillStyle = "yellow";
            else if(this.type == "health")
            {
                var self = this;
               // var speedImg = new Image();
              //  speedImg.src = 'popohp.png';
               // speedImg.onload = function()
               // {
                    self.context.drawImage(img3,self.x,self.y,self.width,self.height);
               // }
            }

           // this.context.fillRect(this.x,this.y,this.width,this.height);
        }
    };
}

function LifeBar(x,y,w,h,ninja,name,context)
{
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.ninja = ninja;
    this.context = context;
    this.name = name;

    this.draw = function()
    {
        this.context.strokeStyle ="black";
        this.context.lineWidth = 5;
        this.context.strokeRect(this.x,this.y,this.w,this.h);

        this.context.fillStyle = "red";
        this.context.fillRect(this.x,this.y,this.w,this.h);

        this.context.fillStyle = "green";
        this.context.fillRect(this.x,this.y,this.ninja.life,this.h);

        this.context.fillStyle = "black";
        this.context.font="18px Arial";
        this.context.fillText(this.name,this.x+(108),this.y+this.y/2+10);
    }
}

function Score(x,y,fontSize,context)
{
    this.x = x;
    this.y = y;
    this.fontSize = fontSize;
    this.scoreP1 = 0;
    this.scoreP2 = 0;
    this.context = context;

    this.draw = function()
    {
        this.context.font="40px Arial";
        this.context.fillText(this.scoreP1+":"+this.scoreP2,this.x,this.y);
    };
}

function Menu(context)
{
    this.context = context;
    this.isActif = true;
    this.buttonTab = [];
    this.optTab = [];
    this.answer = null;

    this.drawAfterGameMenu = function(winner)
    {
        this.buttonTab = [];

        this.context.fillStyle = "white";
        this.context.fillRect(1024/2-304,768/2-154,608,308);

        this.context.fillStyle = "black";
        this.context.fillRect(1024/2-300,768/2-150,600,300);

        this.add(new Button(1024/2-200,250,400,50,"Menu",0,this.context));
        this.add(new Button(1024/2-200,330,400,50,"Restart",0,this.context));

        this.context.fillStyle = "white";
        this.context.fillText("Winner : "+winner,1024/2-100,450);

        for (var i = 0; i <= this.buttonTab.length - 1; i++)
            this.buttonTab[i].draw();
    };

    this.drawPause = function()
    {
        this.buttonTab = [];

        this.context.fillStyle = "white";
        this.context.fillRect(1024/2-304,768/2-154,608,308);

        this.context.fillStyle = "black";
        this.context.fillRect(1024/2-300,768/2-150,600,300);

        this.add(new Button(1024/2-200,250,400,50,"Menu",0,this.context));
        this.add(new Button(1024/2-200,350,400,50,"Restart",0,this.context));
        this.add(new Button(1024/2-200,450,400,50,"Resume",0,this.context));

        for (var i = 0; i <= this.buttonTab.length - 1; i++)
            this.buttonTab[i].draw();
    };
    
    this.draw = function()
    {
        this.buttonTab = [];

        this.add(new Button(1024/2-200,250,400,50,"Play",0,this.context));
        this.add(new Button(1024/2-200,350,400,50,"Option",1,this.context));
        this.add(new Button(1024/2-200,450,400,50,"Rules",2,this.context));
        
        //background
        this.context.fillStyle = "black";
        this.context.fillRect(0,0,1024,768);

        var self = this;
        var monImg = new Image();
        monImg.src = 'ninjaLogo.png';
        monImg.onload = function(e)
        {
            self.context.drawImage(this,150,30);
        }

        for (var i = 0; i <= this.buttonTab.length - 1; i++)
            this.buttonTab[i].draw();
    };

    this.drawOption = function()
    {
        
    };

    this.add = function(Button)
    {
        this.buttonTab.push(Button);
    };
}

function Button(x,y,w,h,text,op,context)
{
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.isHover = false;
    this.context = context;
    this.text = text;
    this.clickOp = op;
    this.soundCount = 0;

    this.draw = function()
    {
        if(this.isHover)
        {
            this.context.strokeStyle ="white";
            this.context.lineWidth = 5;
            this.context.strokeRect(this.x,this.y,this.w,this.h);

            this.context.fillStyle = "white";
            this.context.fillRect(this.x,this.y,this.w,this.h);

            this.context.fillStyle = "black";
            this.context.font="25px Arial";
            this.context.fillText(this.text,this.x+170-this.text.length,this.y+35);

            if(this.soundCount == 0)
            {
                var audio= new Audio('button-3.mp3');
                audio.play();
            }

            this.soundCount++;
        }
        else
        {
            this.context.strokeStyle ="white";
            this.context.lineWidth = 5;
            this.context.strokeRect(this.x,this.y,this.w,this.h);

            this.context.fillStyle = "black";
            this.context.fillRect(this.x,this.y,this.w,this.h);

            this.context.fillStyle = "white";
            this.context.font="25px Arial";
            this.context.fillText(this.text,this.x+170-this.text.length,this.y+35);
            this.soundCount = 0;
        }
    }
}

function GameResult(p1s,p2s)
{
    this.id = 0;
    this.p1s = p1s;
    this.p2s = p2s;
    this.dateObj = new Date();
    this.month = this.dateObj.getUTCMonth() + 1; //months from 1-12
    this.day = this.dateObj.getUTCDate();
    this.year = this.dateObj.getUTCFullYear();
    this.date = this.year + "/" + this.month + "/" + this.day;
}

function ResultList()
{
    this.rList = [];

    this.add = function(GameResult)
    {
        var getS = localStorage.getItem("rList");
        
        if(getS != null && getS != "")
            this.rList = this.getList();

        this.rList.push(GameResult);
        localStorage.setItem("rList",JSON.stringify(this.rList));

        var getUl = document.getElementById("ul");

        var newLi = document.createElement("li");
        newLi.setAttribute("class","lis");

        var scoreP1 = document.createElement("span");
        scoreP1.innerHTML = "Player 1 : "+GameResult.p1s;

        var scoreP2 = document.createElement("span");
        scoreP2.innerHTML = "Player 2 : "+GameResult.p2s;

        var date = document.createElement("span");
        date.innerHTML = "Partie jouer a cette date --> "+GameResult.date;

        newLi.appendChild(scoreP1);
        newLi.appendChild(scoreP2);
        newLi.appendChild(date);
        getUl.appendChild(newLi);
    };

    this.getList = function()
    {
        return JSON.parse(this.rList = localStorage.getItem("rList"));
    };

    this.afficher = function()
    {
        var getS = localStorage.getItem("rList");

        if(getS != null && getS != "")
        {
            this.rList = this.getList();

            var getUl = document.getElementById("ul");
            
            for(var i = 0 ; i <= this.rList.length-1 ; i++)
            {
                var newLi = document.createElement("li");
                newLi.setAttribute("class","lis");

                var scoreP1 = document.createElement("span");
                scoreP1.innerHTML = "Player 1 : "+this.rList[i].p1s+" ";
                
                var scoreP2 = document.createElement("span");
                scoreP2.innerHTML = "Player 2 : "+this.rList[i].p2s+" ";
                
                var date = document.createElement("span");
                date.innerHTML = "Partie jouer a cette date --> "+this.rList[i].date;
                
                newLi.appendChild(scoreP1);
                newLi.appendChild(scoreP2);
                newLi.appendChild(date);
                getUl.appendChild(newLi);
            }
        }
    };
}