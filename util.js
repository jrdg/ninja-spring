/**
 * Created by Jordan Gauthier on 2016-07-01.
 */
function Point(x,y)
{
    this.x = x;
    this.y = y;
}


function Rectangle(x,y,width,height,context,isFill,color,isStroke,colorStroke)
{
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.color = color;
    this.context = context;
    this.isStroke = isStroke;
    this.colorStroke = colorStroke;
    this.isFill = isFill;
    this.lineWidth = 1;

    this.draw = function()
    {
        this.context.lineWidth = this.lineWidth;
        if(this.isStroke)
        {
            this.context.strokeStyle = this.colorStroke;
            this.context.strokeRect(this.x,this.y,this.width,this.height);
        }

        if(this.isFill)
        {
            this.context.fillStyle = this.color;
            this.context.fillRect(this.x,this.y,this.width,this.height);
        }
    };

    this.setX = function(x)
    {
        this.x = x;
    };

}

function Line(startx,starty,endx,endy,context,color)
{
    this.startx = startx;
    this.starty = starty;
    this.endx = endx;
    this.endy = endy;
    this.context = context;
    this.color = color;
    this.lineWidth = 1;

    this.draw = function()
    {
        this.context.beginPath();
        this.context.strokeStyle = this.color;
        this.context.lineWidth = this.lineWidth;
        this.context.moveTo(this.startx,this.starty);
        this.context.lineTo(this.endx,this.endy);
        this.context.stroke();
        this.context.closePath();
    }
}

function Triangle(p1,p2,p3,context,color)
{
    this.context = context;
    this.p1 = p1;
    this.p2 = p2;
    this.p3 = p3;
    this.color = color;

    this.draw = function()
    {
        this.context.beginPath();
        this.context.strokeStyle = this.color;
        this.context.moveTo(this.p1.x,this.p1.y);
        this.context.lineTo(this.p2.x,this.p2.y);
        this.context.lineTo(this.p3.x,this.p3.y);
        this.context.lineTo(this.p1.x,this.p1.y);
        this.context.stroke();
        this.context.closePath()
    }
}

function Oval(x,y,width,startA,endA,isFill,colorFill,isStroke,colorStroke,context)
{
    this.context = context;
    this.x = x;
    this.y = y;
    this.width = width;
    this.startA = startA;
    this.endA = endA;
    this.isFill = isFill;
    this.colorFill = colorFill;
    this.isStroke = isStroke;
    this.colorStroke = colorStroke;
    this.lineWidth = 1;

    this.draw = function()
    {
        this.context.beginPath();
        this.context.lineWidth = this.lineWidth;
        this.context.strokeStyle = "red";
        this.context.arc(this.x,this.y,this.width/2,this.startA,this.endA);

        if(this.isFill)
        {
            this.context.fillStyle = this.colorFill;
            this.context.fill();
        }

        if(this.isStroke)
        {
            this.context.strokeStyle = this.colorStroke;
            this.context.stroke();
        }

        this.context.closePath();
    }
}


//recupere les coordoner de langle de la fin
function getPoint(c1,c2,radius,angle)
{
    return [c1+Math.cos(angle)*radius,c2+Math.sin(angle)*radius];
}
